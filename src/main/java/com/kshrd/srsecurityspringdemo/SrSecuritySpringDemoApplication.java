package com.kshrd.srsecurityspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrSecuritySpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrSecuritySpringDemoApplication.class, args);
    }

}
