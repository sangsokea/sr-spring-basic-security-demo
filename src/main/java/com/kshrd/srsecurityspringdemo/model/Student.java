package com.kshrd.srsecurityspringdemo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Accessors (chain = true)
public class Student {

    private int id;
    private String username;
    private  String gender;
    private String password;
    private String bio;

}
