package com.kshrd.srsecurityspringdemo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthUser {

    private  int id;
    private String username;
    private String password;
    // Roles
    private List<String> roles;
}
