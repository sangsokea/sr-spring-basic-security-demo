package com.kshrd.srsecurityspringdemo.repository;


import com.kshrd.srsecurityspringdemo.model.AuthUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentRepository {


   @Select("select * from students where username=#{username}")
   @Results(
           {

                   @Result(property = "roles",column = "id",many = @Many(select = "findRoleByUserId"))
           }
   )
   AuthUser findUserByUsername(String username);

   @Select("select role_name from roles inner join student_role sr on roles.id = sr.role_id where user_id =#{id}")
   List<String> findRoleByUserId(int id);



}
