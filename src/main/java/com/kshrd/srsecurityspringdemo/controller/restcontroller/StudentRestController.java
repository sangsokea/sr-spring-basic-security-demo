package com.kshrd.srsecurityspringdemo.controller.restcontroller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentRestController {


    @GetMapping("/public")
    public String normaldata(){
        return  "This is public method for everyone  ";
    }


    @GetMapping("/foruser")
    public String forNormalUser(){
        return  "Hi ! So you are the normal user! ";
    }

    @GetMapping("/foradmin")
    public String anotherTwo(){
        return  "Hello Admin!";
    }


    @GetMapping("/forhandsome")
    public String forHandsome(){
        return  "This method is for handsome users...";
    }
}
